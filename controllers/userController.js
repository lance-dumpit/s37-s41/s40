const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//User Registration
module.exports.registerUser = (reqBody) => {
	
		let newUser = new User({
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10)
		})

		//Saves the created object to our database
		return newUser.save().then((user, error) => {

			//Registration failed
			if(error){
				return false

			//Registration successful 
			} else {
				return true
			}
		})
}


//User Authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result =>{

		if(result == null){
			return false
		} else {

			//compareSync(dataToBeCompared, encryptedData)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			} else {
				return false;
			}
		}
	})
}

// Set User as Admin

module.exports.setAsAdmin = (data) => {
	console.log(data)

	return User.findById(data.adminUserId).then((result, error) => {

		if(error) {

			return false
		} else {
			// if user is an admin
			if(result.isAdmin){
				return User.findByIdAndUpdate(data.updateToAdmin, {isAdmin: true}).then((result, error) => {

					if (error){
						return false
					} else {

						return true
					}

				})
			} 

		}
	})
}
		
//Create Order (Non-admin only)

module.exports.addOrder = async(data) => {

console.log(data)

	if(data.isAdmin === true){

		 return false

	} else {

		let isUserUpdated = await User.findById(data.userId).then(user => {

			user.purchases.push({productId : data.productId})
			
			return user.save().then((user, error) => {

				if(error){

					return false

				} else {

					return true
				}
			})
		})

		let isProductUpdated = await Product.findById(data.productId).then(product => {

			product.purchasers.push({userId : data.userId})

			return product.save().then((product, error) => {

				if(error){

					 return false

				} else {

					return true
				}
			})
		})

		if(isUserUpdated && isProductUpdated){

			return true

		} else {

			return false
		}
	}
}


// Retrieve all orders(Admin only)

module.exports.viewAllOrders = (data) => {

	return User.find().then(result => {

		if(data.isAdmin == true){

			return result

		} else {
			return false
		}
	})
}

//Retrieve authenticated user's orders(Non-Admin only)

module.exports.viewAuthUserOrders = (data) => {
console.log(data)

	if(data.isAdmin == true){

		return false
	} else {
		return User.findById(data.userId).then((result, error) => {
			console.log(data)

			if(data.isAdmin == false) {
				return result.purchases
			} else {
				return false
			}
		})
	}
}