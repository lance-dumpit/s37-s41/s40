const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

//Route for creating a product
router.post("/add", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)
    productController.addProduct(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))

});


//Route for retrieving all active products
router.get("/", (req, res) => {

    productController.getAllActive().then(resultFromController => res.send(resultFromController))
});


//Route for retrieving a single product
router.get("/:productId", (req, res) => {
    console.log(req.params)

    productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));

})

//Route for updating a product information
router.put("/:productId", auth.verify, (req, res) => {

    const data = {
        productId: req.params.productId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        updatedProduct: req.body
    }
    
    productController.updateProduct(data).then(resultFromController => res.send(resultFromController));

})


//Route for archiving a product
router.put('/:productId/archive', auth.verify, (req, res) => {

    const data = {
        productId : req.params.productId,
        payload : auth.decode(req.headers.authorization).isAdmin
    }

    productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))
});







// Allows us to export the "router" obejct that will be accessed
module.exports = router;