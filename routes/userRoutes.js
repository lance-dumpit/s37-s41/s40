const express = require("express");
const userController = require("../controllers/userController");
const auth = require("../auth"); 
const router = express.Router();


// Route for User Registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(
		resultFromController));

});


// Route for User Authentication
router.post("/login", (req,res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})



//Route for setting user as Admin

router.put("/:userId/setAsAdmin",auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)

	const data = {

		updateToAdmin: req.params.userId, //user ID to set as admin
		adminUserId: userData.id //admin user ID that is logged in 
			
	}

	userController.setAsAdmin(data).then(resultFromController => res.send(resultFromController));
})


// Route for creating order (Non-admin only)

router.post("/checkout", auth.verify, (req, res) => {

	const data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		productId : req.body.productId
	}

	userController.addOrder(data).then(resultFromController => res.send(resultFromController))
})

//Route for retrieving all orders(Admin only)

router.get("/orders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.viewAllOrders(userData).then(resultFromController => res.send(resultFromController))
})

//Route for retrieving authenticated user's orders(Non-Admin only)

router.get("/myOrders", auth.verify, (req, res) => {
	
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.viewAuthUserOrders(data).then(resultFromController => res.send(resultFromController))
})


















// Allows us to export the "router" obejct that will be accessed
module.exports = router