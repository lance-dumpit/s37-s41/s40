const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	// The "purchases" property/field will be an array of objects containing the orders, the date and time that the user purchase to the course and the status that indicates if the user is currently enrolled to a course
	purchases: [
		{	
			productId : {
				type : String,
				required : [true, "Product ID is required"]
			},
			purchasedOn : {
				type : Date,
				default : new Date()
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);